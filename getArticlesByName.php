<?php

/**
 * @var $url String Server Url
 * @var $search String String which should be searched for
 */


//TODO: don't show items that are already added
$opts = array('http' =>
	array(
		'method'  => 'GET',
		'header'  => 'Content-type: application/x-www-form-urlencoded'
	)
);

$context  = stream_context_create($opts);

$url2 = 'http://'.$url.'/api/v2/articles.json?api_key=123456&q='.$search;
$fp = file_get_contents($url2, false, $context);

if (!$fp) {
	echo "Error - Could not read Articles<br />";
	echo $url;
} else {
	$data = json_decode($fp);

	$url = '<a href=\'index.php?url='.$url.'&article_id=%s\'>%s</a><br />';
	foreach($data as $row) {
		echo sprintf($url, $row->id, $row->name);
	}
}