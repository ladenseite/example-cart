<?php

$opts = array('http' =>
	array(
		'method'  => 'DELETE',
		'header'  => 'Content-type: application/x-www-form-urlencoded'
	)
);

$context  = stream_context_create($opts);

$url = 'http://'.$url.'/api/v2/carts/'.$_SESSION['cart_id'].'/items/'.$remove_article_id.'.json?api_key=123456';
$fp = file_get_contents($url, false, $context);

if (!$fp) {
	echo "Error - Could not remove Article<br />";
	echo $url."<br />";
} else {
	echo "Article successfully removed!<br />";
}