<?php

session_start();


if(checkGetValue('reset_cart')) {
	unset($_SESSION['cart_id']);
}

error_reporting(0);

//At first we will add a cart. This cart (cart_id) is being saved into the session.

$url = checkGetValue('url');
if(empty($url)) {
	?>
		url has to be set (eg. laden.local or localhost)
		<form class="navbar-form navbar-left" role="search">
			<div class="form-group">
				URL: http://<input type="text" class="form-control" placeholder="url" name="url" value="">
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	<?php
	die;
}

$search = checkGetValue('search');
$article_id = checkGetValue('article_id');
$debug = checkGetValue('debug');
$remove_article_id = checkGetValue('remove_article_id');

createCart($url);

$current_url = explode("?", $_SERVER['REQUEST_URI']);

?>


<!DOCTYPE html>
<html lang="en">
	<head>
	
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-inverse" role="navigation">
					<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
					<!--      <a class="navbar-brand" href="#">Brand</a>-->
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<form class="navbar-form navbar-left" role="search">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search" name="search" value="<?php echo $search; ?>">
									<input type="hidden" name="url" value="<?php echo $url; ?>">
								</div>
								<button type="submit" class="btn btn-default">Search</button>
							</form>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="<?php echo $current_url[0].'?reset_cart=true&url='.$url; ?>">Reset Cart</a></li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<h2>Warenkorb</h2>
					<?php
						if(!empty($article_id)) {
							addArticleToCart($url, $article_id);
						}
						if(!empty($remove_article_id)) {
							removeArticleFromCart($url, $remove_article_id);
						}
						
						showCart($url); 
					?>
				</div>
				<div class="col-xs-8">
					<h2>Artikel</h2>
					<?php
						if(empty($search)) {
							getRandomArticles($url);
						} else {
							getArticlesByName($url, $search);
						}
					?>
				</div>
			</div>
		</div>
	</body>
</html>

<?php


debug();


function createCart($url) {
	if(!isset($_SESSION['cart_id'])) {
		include 'addCart.php';
	}
}

function debug() {
	if(isset($_GET['debug'])) {
		echo "<pre>".print_r($_SESSION, true)."</pre>";
	}
}

function getRandomArticles($url) {
	include 'getRandomArticles.php';
}

function getArticlesByName($url, $search) {
	include 'getArticlesByName.php';
}

function checkGetValue($val) {
	return isset($_GET[$val])?$_GET[$val]:null;
}

function addArticleToCart($url, $article_id) {
	include 'addArticleToCart.php';
}

function showCart($url) {
	include 'getCart.php';
}

function removeArticleFromCart($url, $remove_article_id) {
	include 'removeItemFromCart.php';
}

?>
