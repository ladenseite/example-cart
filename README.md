# Example API Implementation #

## Installation ##

Checkout this repository into any folder on you webserver. Start the index.php with your browser and type in the URL you want to connect to (API url).

## Usage ##

At startup, a new cart will be created and the id stored to the session. You can add articles to the cart by clicking on the name in the articles list. You can also remove articles from the cart by clicking on the name in the Cart List. To search for an article, use the text field on the top.

### Parameters ###

A list of get parameters you can append to your url:

* debug=true - shows debug information at the end of the page
* reset_cart=true - unsets the cart id stored in the session
* article_id=$$$ - adds the given article to the cart
* remove_article_id=$$$ - removes given article from the cart
* search=$$$ - searches the article names for the search string

## Technical Details ##

Every Api Call is put in a single file for visibility