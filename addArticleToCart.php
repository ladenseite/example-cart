<?php

$opts = array('http' =>
	array(
		'method'  => 'POST',
		'header'  => 'Content-type: application/x-www-form-urlencoded'
	)
);

$context  = stream_context_create($opts);

$url = 'http://'.$url.'/api/v2/carts/'.$_SESSION['cart_id'].'/articles/'.$article_id.'.json?api_key=123456';
$fp = file_get_contents($url, false, $context);

if (!$fp) {
	echo "Error - Could not save Article<br />";
	echo $url."<br />";
} else {
	echo "Article successfully saved!<br />";
}