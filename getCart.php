<?php

	$opts = array('http' =>
		array(
			'method'  => 'GET',
			'header'  => 'Content-type: application/x-www-form-urlencoded'
		)
	);

	$context  = stream_context_create($opts);

	$url2 = 'http://'.$url.'/api/v2/carts/'.$_SESSION['cart_id'].'.json?api_key=123456';
	$fp = file_get_contents($url2, false, $context);

	if (!$fp) {
		echo "Error - Could not read Articles<br />";
		echo $url;
	} else {
		$data = json_decode($fp);

		$url = '<a href=\'index.php?url='.$url.'&remove_article_id=%s\'>%s</a><br />';
		foreach($data->items as $row) {
			echo sprintf($url, $row->id, $row->article->name);
		}
		echo "<br />";
		echo "<a href='".$data->url."'>Checkout</a>";
	}