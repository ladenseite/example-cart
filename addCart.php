<?php

/**
 * @var $url String Server Url
 * @var $search String String which should be searched for
 */

$opts = array('http' =>
	array(
		'method' => 'POST',
		'header' => 'Content-type: application/x-www-form-urlencoded'
	)
);

$context = stream_context_create($opts);

$url2 = 'http://' . $url . '/api/v2/carts.json?api_key=123456';
$fp = file_get_contents($url2, false, $context);

if (!$fp) {
	echo "Error - Could not create Cart<br />";
	echo $url;
} else {
	$data = json_decode($fp);
	$_SESSION['cart_id'] = $data->id;
}
